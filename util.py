import doctest

class FileSet:

    def __init__(self, filenames):
        self.filenames = filenames

    def __iter__(self):
        r"""Iterate lines in the files.

        Example:

        >>> fileset = FileSet(["stress.py", "util.py"])
        >>> lines = list(fileset)
        >>> 'class FileSet:\n' in lines
        True
        """
        for filename in self.filenames:
            print(filename)
            with open(filename, 'r', encoding='utf-8') as fd:
                for line in fd:
                    yield line

if __name__ == "__main__":
    print(doctest.testmod())
