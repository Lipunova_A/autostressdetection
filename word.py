# -*- coding: utf-8 -*-
import doctest
import re
import sys
from collections import Counter

vowels = 'аеёиоуыэюя'
consonants = 'бвгджзйклмнпрстфхцчшщъ'
voiced_sibilant = 'жщц'
voiceless_sibilant = 'чш'

def expand(string):
  return string.format(**globals())

substitutions = [(expand('^[{vowels}]'), {
                        'а': 'о',
                        'о': 'а',
                        'э': 'еиы',
                        'я': 'еи',
                        'е': 'ия',
                        'и': 'еэ',
                        }),
                 (expand('[{vowels}]$'), {
                     'о': 'а',
                     }),
                 (expand('(?<=[{voiced_sibilant}])[{vowels}]'), {
                     'о': 'а',
                     'и': 'еыэ',
                     'э': 'еиы',
                     'е': 'иыэ',
                     }),
                 (expand('(?<=[{voiceless_sibilant}])[{vowels}]'), {
                     'а': 'еиэ',
                     'о': 'аеиы',
                     'и': 'е',
                     'е': 'и',
                     }),
                 (expand('(?<=[{consonants}])[{vowels}]'), {
                     'а': 'о',
                     'о': 'а',
                     'ы': 'э',
                     'е': 'и',
                     'и': 'е',
                     'я': 'еи',
                     }),
                 ]

class Word(object):
    """Word from corpus.

    Attributes:

    * `words` - alternative instances of the word with their frequencies
    * `clean_word` - the word lowercased with duplications removed
    * `frequency` - number of occurences in the corpus
    * `pos` - part of speech
    * `stem` - word without inflection ending
    * `stressed_word` - lowercase word with stress represented as uppercase, None if not detected yet
    * `criteria` - criteria used for stress detection
    """
    stem = None
    stressed_word = None
    criteria = None

    def __init__(self, word, pos, frequency=0):
        self.words = Counter(dict(word=frequency))
        self.clean_word = self.normalize_word(word)
        self.pos = pos

    @staticmethod
    def normalize_word(word):
        """Normalize word: lowercase, remove duplications, etc..."""
        # XXX remove duplications
        return word.lower()

    def add(self, token):
        """Increase the frequency counter for the given token.
        """
        self.words[token] += 1

    def consonant_mask(self, word=None):
        """Return lowercased word with each vowel replaced with "*".

        Example:
        >>> w = Word('дЛИнношеее', 'A')
        >>> w.consonant_mask()
        'дл*нн*ш***'
        >>> w.consonant_mask('длинНОше')
        'дл*нн*ш*'
        """
        if word is None:
            word = self.clean_word
        return re.sub(expand(r'[{vowels}]'), '*', self.normalize_word(word))

    def detect_one_syllable(self):
        """Detect stress if word consists of a single syllable. Return self.

        Example:
        >>> Word('шВаХ', 'ADJ').detect_one_syllable().stressed_word
        'швАх'
        >>> Word('барабулька', 'S').detect_one_syllable().stressed_word is None
        True
        """
        word = self.clean_word
        if re.search(expand('^[^{vowels}]*[{vowels}][^{vowels}]*$'), word):
            for vowel in vowels:
                if vowel in word:
                    self.stressed_word =  re.sub(expand('(.*)[{vowels}](.*)'),
                                                 r'\1' + vowel.upper() + r'\2',
                                                 word)
        return self

    def detect_yo(self):
        """Detect stress if word contains yo. Return self.

        Example:
        >>> Word('шАТЁр', 'S').detect_yo().stressed_word
        'шатЁр'
        >>> Word('стук', 'S').detect_yo().stressed_word is None
        True
        >>> Word('Трёмсё', 'S').detect_yo().stressed_word is None
        True
        """
        if re.search('.*ё.*ё', self.clean_word) == None:
            if 'ё' in self.clean_word:
                self.stressed_word = re.sub('ё', 'Ё', self.clean_word)
        return self

    def detect_stretched(self):
        """Detect stress if word contains stretched vowel. Return self.

        Example:
        >>> Word('веДрооо', 'S').detect_stretched().stressed_word
        'ведрО'
        >>> Word('веДро-о-о', 'S').detect_stretched().stressed_word
        'ведрО'
        >>> Word('вёдра', 'S').detect_stretched().stressed_word is None
        True
        """
        word = re.sub('-', '', self.clean_word)
        for vowel in vowels:
            while re.search(vowel+'{3,}', word) != None:
                check = re.search('(.*)(' + vowel + '{3,})(.*)', word)
                word = check.group(1) + vowel.upper() + check.group(3)
                self.stressed_word = word
        return self

    def has_misspelling(self, other):
      # добавить невозможность замены всех гласных
        """Tell if `other` might be misspelled version of `self`.

        Example:

        >>> Word('япония', 'S').has_misspelling(Word('епонея', 'S'))
        True

        Some misspelling rules are not commutative:

        >>> Word('жонглировать', 'S').has_misspelling(Word('жанглировать', 'S'))
        True
        >>> Word('жанглировать', 'S').has_misspelling(Word('жонглировать', 'S'))
        False
        """
        for position, letter in enumerate(self.consonant_mask()):
            if letter == '*':
                if not self.substitution_possible(other, position):
                    return False
        return True

    def substitution_possible(self, other, position):
        """Tell if given position in `other` might be in misspelled version of `self`.

        Example:

        >>> Word('япония', 'S').substitution_possible(Word('епония', 'S'), 0)
        True

        Some misspelling rules are not commutative:

        >>> Word('жонглировать', 'S').substitution_possible(Word('жанглировать', 'S'), 1)
        True
        >>> Word('жанглировать', 'S').substitution_possible(Word('жонглировать', 'S'), 1)
        False

        Only words of the same POS might be misspelled versions of each other:

        >>> Word('больной', 'S').substitution_possible(Word('бальной', 'S'), 1)
        True
        >>> Word('больной', 'S').substitution_possible(Word('бальной', 'A'), 1)
        False

        The words for comparison must have the same consonant mask:

        >>> Word('япония', 'S').substitution_possible(Word('японец', 'S'), 0)
        Traceback (most recent call last):
          ...
        AssertionError: Words must have the same consonant mask
        """
        if self.pos != other.pos:
            return False
        assert self.consonant_mask() == other.consonant_mask(), \
          "Words must have the same consonant mask"
        word = self.clean_word
        other_word = other.clean_word
        for precondition, substitution in substitutions:
            if re.compile(precondition).search(word, position, position+1):
                possible_substitutions = substitution.get(word[position], '')
                if other_word[position] in possible_substitutions + word[position]:
                    return True
                else:
                    return False
        assert False, "No precondition matched"

    def substitution_possible_plenty(wordset, position):
      vowels_classified = ''
      result = []
      for word_object, index in enumerate(wordset):
        if word_object.clean_word[index] not in vowels_classified:
          result.append([word_object])
          vowels_classified += word_object_other.clean_word[position]
          for precondition, substitution in substitutions:
            if re.compile(precondition).search(word_object.clean_word, position, position+1):
              possible_substitutions = substitution.get(word_object.clean_word[position], '')
              for word_object_other in wordset[index:]:
                if word_object_other.clean_word[position] in possible_substitutions + word_object.clean_word[position]:
                  result[-1].append[word_object_other]
                  vowels_classified += word_object_other.clean_word[position]
      return result

    @property
    def frequency(self):
        """Return the total frequency of all word instances.
        """
        return sum(self.words.values())

    def __str__(self):
        """Return the original word.

        Example:
        >>> print(Word('СаМаЛёт', 'S'))
        самалёт
        """
        return self.clean_word

    def __repr__(self):
        """Return unambiguous representation of self.

        Example:
        >>> Word('СаМалёт', 'S')
        <Word(word='СаМалёт', pos='S', frequency=0)>
        """
        return "<Word(word={}, pos={}, frequency={})>".format(
            repr(self.clean_word), repr(self.pos), self.frequency
        )

    def __lt__(self, other):
        """Comparison based on clean_word and POS, case-sensitive.

        >>> Word("самолёт", "S") < Word("самолёт", "S")
        False
        >>> Word("самолёт", "S") < Word("сепулька", "S")
        True
        >>> Word("СаМоЛёТ", "S") < Word("самолёт", "S")
        False
        >>> Word("самолёт", "A") < Word("самолёт", "S")
        True
        """
        if self.clean_word == other.clean_word:
            return self.pos < other.pos
        else:
            return self.clean_word < other.clean_word

    def __eq__(self, other):
        """Comparison based on clean_word and POS, case-sensitive.

        >>> Word("самолёт", "S") == Word("самолёт", "S")
        True
        >>> Word("самолёт", "S") == Word("сепулька", "S")
        False
        >>> Word("СаМоЛёТ", "S") == Word("самолёт", "S")
        True
        >>> Word("самолёт", "S") == Word("самолёт", "A")
        False
        """
        return self.clean_word == other.clean_word and self.pos == other.pos

    def __hash__(self):
        """Hash value based on clean_word and POS.

        >>> hash(Word("самолёт", "S")) == hash(Word("самолёт", "S"))
        True
        >>> hash(Word("самолёт", "S")) == hash(Word("сепулька", "S"))
        False
        >>> hash(Word("СаМоЛёТ", "S")) == hash(Word("самолёт", "S"))
        False
        >>> hash(Word("самолёт", "S")) == hash(Word("самолёт", "A"))
        False
        """
        return hash((self.clean_word, self.pos))

class WordSet(Word):
    """Set of words having the same set of consonants and vowel positions.

    Attributes:

    * `words` - a list of words
    * `frequency` - total occurence in the corpus
    * `pos` - inferred part of speech
    * `stem` - inferred stem
    * `stressed_word` - inferred correct spelling, stress represented with capitalization
    * `criteria` - criteria used for stress detection
    """

    def __init__(self, words=None):
        self.words = words or []

    def detect_stress(self):
        """Detect stressed positions in word and sets `stressed_word` and `criteria`.

        """
        # XXX: unfinished
        # TODO: call detect_* methods of each word in the set
        self.stress_by_misspellings()

    def stress_by_misspellings(self):
        """Detect stressed positions in word based on misspellings.
        """
        # XXX: unfinished
        # TODO: documentation, tests
        alternatives = [
            (position, set(word.clean_word[position] for word in self.words))
            for position in range(len(self.words[0].clean_word))
        ]
        unaltered_vowels = [ (position, list(letters)[0])
            for position, letters in alternatives
            if len(letters) == 1 and list(letters)[0] in vowels
        ]
        if len(unaltered_vowels) == 1:
            position, letter = unaltered_vowels[0]
            word = max(self.words, key=lambda word: word.frequency).clean_word
            self.stressed_word = word[:position].lower() + word[position].upper() + word[position+1:].lower()
            self.criteria = 'misspellings'

    @property
    def frequency(self):
        """Return total frequency of all words in the wordset.

        Example:
        >>> wordset = WordSet()
        >>> wordset.words.append(Word('косой', 'S', frequency=1))
        >>> wordset.words.append(Word('кАсой', 'S', frequency=1))
        >>> wordset.words.append(Word('Косой', 'S', frequency=1))
        >>> wordset.frequency
        3
        """
        return sum(word.frequency for word in self.words)

    def __iter__(self):
        """Iterate words in `self`.

        Example:
        >>> for word in WordSet([Word('собака', 'S'), Word('бывает', 'S')]):
        ...     print(word)
        собака
        бывает
        """
        yield from self.words

if __name__ == "__main__":
    print(doctest.testmod())
