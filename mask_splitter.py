"""Inventory for splitting WordSet into several based on Word masks."""
import word
import corpus

def split(initial_wordset):
    """Split wordset into several. Return the resulting wordsets."""
    wordsets = [initial_wordset]
    for position, letter in enumerate(initial_wordset.words[0].word):
        if letter in 'аеёиоуыэюя':
            current_wordsets = []
            for wordset in wordsets:
                current_wordsets += word.substitution_possible_plenty(wordset, position)
        wordsets = current_wordsets
        
    if len(worsets) > 1:
        corpus.word_sets[wordsets[0][0].consonant_mask()] = []
        for wordset in wordsets:
            word_set = word.WordSet()
            for word_object in wordset:
                word_set.words.append(word_object)
            corpus.word_sets[wordsets[0][0].consonant_mask()] += word_set
            
    
