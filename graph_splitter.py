"""Inventory for splitting WordSet into several by graph properties.

Version 1. Assumes a clique is likely a good WordSet.
"""
import doctest
import word

def split(wordset):
    """Split wordset into several. Return the resulting wordsets.

    Example:
    >>> words = "аптеке,аптеки,аптеку,оптика,оптике,оптики,оптику"
    >>> wordset = word.WordSet([word.Word(w, 'S') for w in words.split(",")])
    >>> print_cliques(split(wordset))
    аптеке аптеки оптике оптики
    аптеку оптику
    оптика
    """
    graph = wordset_graph(wordset)
    for clique in bron_kerbosch(graph):
        yield word.WordSet(list(clique))

def wordset_graph(wordset):
    """Represent wordset as a graph of possibly misspelled words.

    >>> from word import Word, WordSet
    >>> wordset = WordSet([Word('аптека', ''), Word('оптека', ''),
    ...     Word('оптика', ''), Word('аптеко', '')])
    >>> print_graph(wordset_graph(wordset))
    аптека -> аптеко оптека оптика
    аптеко -> аптека оптека оптика
    оптека -> аптека аптеко оптика
    оптика -> аптека аптеко оптека
    """
    graph = {}
    for word in wordset.words:
        graph[word] = []
        for other in wordset.words:
            if other is word:
                continue
            if word.has_misspelling(other):
                graph[word].append(other)
    return graph

def bron_kerbosch(graph, clique=(), candidates=None, seen=frozenset()):
    """Iterate over all maximal cliques in a graph.

    Example Wikipedia graph:

    >>> graph = {'1':'25', '2':'135', '3':'24', '4':'356', '5':'124', '6':'4'}
    >>> print_cliques(bron_kerbosch(graph))
    1 2 5
    2 3
    3 4
    4 5
    4 6
    """
    if candidates is None:
        candidates = set(graph)
    if not candidates and not seen:
        yield clique
    for vertex in loop(candidates):
        neighbors = set(graph[vertex])
        new_candidates = (candidates | {vertex}) & neighbors
        for subclique in bron_kerbosch(
                graph, clique + (vertex,), new_candidates, seen & neighbors):
            yield subclique
        seen = seen | {vertex}

def loop(items):
    """Destructive loop over hash-based container.

    Allows modifiaction of iterated container within the iteration.

    Destructiveness:

    >>> letters = set("abcd")
    >>> sorted(loop(letters))
    ['a', 'b', 'c', 'd']
    >>> letters
    set()

    Allows modification:

    >>> letters = set("abcd")
    >>> result = []
    >>> for letter in loop(letters):
    ...     result.append(letter)
    ...     if letter == "a":
    ...         letters.add("z")
    >>> sorted(result)
    ['a', 'b', 'c', 'd', 'z']
    """
    while items:
        yield items.pop()

def print_graph(graph):
    """Print graph nicely (helper for doctests).

    Example:
    >>> print_graph({'a':'bc', 'd':'ef'})
    a -> b c
    d -> e f
    """
    for node in sorted(graph):
        print(node, '->', *sorted(graph[node]))

def print_cliques(cliques):
    """Print a collection of cliques nicely (helper for doctests).

    Example:
    >>> print_cliques(set(('ab', 'bc', 'cde')))
    a b
    b c
    c d e
    """
    strings = (' '.join(map(str, sorted(clique))) for clique in cliques)
    for string in sorted(strings):
        print(string)

if __name__ == "__main__":
    print(doctest.testmod())
