#!/usr/bin/env python3
import argparse
from corpus import Corpus
from util import FileSet
from graph_splitter import split

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--output', help='...')
parser.add_argument('input', nargs='+', help='...')
args = parser.parse_args()
files = FileSet(args.input)

corpus = Corpus(files)
corpus.create_word_sets()
for parent_word_set in corpus.word_sets.values():
  for word_set in split(parent_word_set):
    word_set.detect_stress()
    if word_set.criteria:
        print(word_set.stressed_word, word_set.criteria, parent_word_set.words, word_set.words)
