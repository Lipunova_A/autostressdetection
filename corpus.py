#!/usr/bin/env python3
# coding: utf-8
from io import StringIO
import doctest
import re
import word

class Corpus:
    """Collection of everything obtained from corpus.

    Attributes:

    * `file` - file-like object used for input
    * `words` - dictionary of {'word': object of class Word}
    * `word_sets` - dictionary of {'consonant mask':
        WordSet object}
    """
    
    def __init__(self, file):
        self.file = file

    def create_words(self):
        """Create dictionary of words with frequencies.
        
        Fill in word morphological info from mystem.

        Example:
        >>> file = StringIO("Косил косой косой косой.")
        >>> corpus = Corpus(file)
        >>> corpus.create_words()
        >>> corpus.words['косой'].word
        'косой'
        >>> corpus.words['косой'].frequency
        3
        """
        self.words = {}
        for token in self.tokens():
            normalized = word.Word.normalize_word(token)
            if normalized not in self.words:
                self.words[normalized] = word.Word(token, 'S')
            self.words[normalized].add(token)

    def create_word_sets(self):
        """Create dictionary of `WordSets` in `self.word_sets`.

        Example:

        >>> file = StringIO("Косил косОй касой Косой.")
        >>> corpus = Corpus(file)
        >>> corpus.create_word_sets()
        >>> corpus.word_sets['к*с*й'].frequency
        3
        >>> sorted([word.word for word in corpus.word_sets['к*с*й'].words])
        ['Косой', 'касой', 'косОй']
        """
        self.create_words()
        self.word_sets = {}
        for word_object in self.words.values():
            if word_object.consonant_mask() not in self.word_sets:
                word_set = word.WordSet()
                word_set.words.append(word_object)
                self.word_sets[word_object.consonant_mask()] = word_set
            else:
                self.word_sets[word_object.consonant_mask()].words.append(word_object)

    def tokens(self):
        """Iterate through tokens of file.

        Example:
        >>> file = StringIO("Казнить,нельзя, помиловать!")
        >>> corpus = Corpus(file)
        >>> list(corpus.tokens())
        ['Казнить', 'нельзя', 'помиловать']
        """
        for line in self.file:
            for token in re.findall(r'\b[ЁА-Яёа-я-]+\b', line):
                yield token
# не нужны?
    @staticmethod
    def line_preprocessing(line):
        """Remove ... from line of text
        """

    @staticmethod
    def normalize_token(token):
        """...
        """

if __name__ == "__main__":
    print(doctest.testmod())
